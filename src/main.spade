use hdmi::main::VgaTiming;
use hdmi::main::VgaOutput;
use hdmi::main::vga_fsm;
use hdmi::main::vga_output;
use hdmi::vga2dvid::DviOut;
use hdmi::vga2dvid::vga2dvid;


struct Output {
    dvi: DviOut,
}


#[no_mangle]
entity vga_drawer(#[no_mangle] vga_clk: clock, rst: bool) -> (VgaOutput, (int<8>, int<8>, int<8>)) {
    let timing = VgaTiming$(
        x_pixels: 1366,
        x_front_porch: 7,
        x_sync_width: 7,
        x_back_porch: 8,
        y_pixels: 768,
        y_front_porch: 4,
        y_sync_width: 4,
        y_back_porch: 4,
    );

    let vga_state = inst vga_fsm$(clk: vga_clk, rst, clk_enable: true, timing);
    let VgaOutput$(hsync, vsync, blank, pixel) = vga_output(vga_state);

    let color = match pixel {
        Some((x,y)) => {
            (trunc(x), trunc(y), if x == y {255u} else {0})
        },
        None => {
            (0,0,0)
        }
    };

    (VgaOutput$(hsync, vsync, blank, pixel), color)
}

entity main(vga_clk: clock, clk_shift: clock, rst: bool) -> Output {
    let (VgaOutput$(hsync, vsync, blank, pixel), color) = inst vga_drawer(vga_clk, rst);

    let dvi = inst vga2dvid$(
        clk_pixel: vga_clk,
        clk_shift,
        rst,
        hsync,
        vsync,
        blank,
        color
    );

    Output $(dvi)
}
