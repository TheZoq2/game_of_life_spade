# top=main::vga_drawer
from spade import SpadeExt
from cocotb import cocotb
from cocotb.clock import Clock
from cocotb.triggers import FallingEdge,Timer 

@cocotb.test()
async def run_2_frames(dut):
    s = SpadeExt(dut)

    clk = dut.vga_clk
    cocotb.start_soon(Clock(clk, 1, units="ns").start())

    s.i.rst = "true"
    await FallingEdge(clk)
    s.i.rst = "false"

    await Timer(1_000_000, units="ns")
